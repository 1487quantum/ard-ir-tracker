//LPF settings (For raw sensor values)
float filterFrequency = 1.0 ;

//IR Eye Pins
const int IRLeds = 2;   // Control IR LED
const int IRLeft = A3;
const int IRRight = A1;
const int IRUp = A0;
const int IRDown = A2;

//Servo pins & config
const int sPan = 5;
const int sTilt = 6;

int sPanMin = 10;    //Min servo val
int sPanMax = 170;   //Maximum servo val

int sTiltMin = 10;    //Min servo val
int sTiltMax = 75;   //Maximum servo val

//Time variable
const long idleTime = 5;    //Number of seconds before entering to idle mode


